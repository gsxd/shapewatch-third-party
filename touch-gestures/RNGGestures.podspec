
Pod::Spec.new do |s|
  s.name         = "RNGGestures"
  s.version      = "1.0.0"
  s.summary      = "RNGGestures"
  s.description  = <<-DESC
                  RNGGestures
                   DESC
  s.homepage     = "https://github.com/author/RNGGestures"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNGGestures.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  