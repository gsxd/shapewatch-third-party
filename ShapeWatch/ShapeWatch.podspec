#
#  Be sure to run `pod spec lint ShapeWatch.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name         = "ShapeWatch"
  spec.version      = "0.9.12"
  spec.summary      = "A short description of ShapeWatch."

  spec.description  = <<-DESC
                   ShapeWatch App for Third Party apps
                   DESC

  spec.homepage     = "http://EXAMPLE/ShapeWatch"
  spec.license      = "MIT"
  spec.module_name   = 'ShapeWatch'
  spec.author        = { "Alejandro Paredes Alva" => "a.alva@globalscanning.com" }
  spec.platform      = :ios, "11.0"
  spec.swift_version = '4.0'
  spec.source       = { :git => "https://bitbucket.org/gsxd/shapewatch-third-party.git", :tag => "#{spec.version}" }
  spec.source_files = "ios/*"
  # spec.resources = ["assets", "viewer"]
  # spec.ios.resource_bundle = { 'ShapeBundle' => 'main.jsbundle' }
  spec.dependency "React"
  spec.dependency "QuantaCorp"
end
