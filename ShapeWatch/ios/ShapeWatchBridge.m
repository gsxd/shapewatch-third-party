//
//  ShapeWatchBridge.m
//  swtestswift
//
//  Created by Alejandro Paredes Alva on 24/07/2019.
//  Copyright © 2019 GlobalScanning. All rights reserved.
//
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(ShapeWatch, RCTEventEmitter)

RCT_EXTERN_METHOD(dismissPresentedViewController:(nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(openViewController:(nonnull NSNumber *)reactTag userId:(nonnull NSString *)userId weight:(nonnull NSNumber *)weight height:(nonnull NSNumber *)height gender:(nonnull NSNumber *)gender)
RCT_EXTERN_METHOD(rnCreateToken)
RCT_EXTERN_METHOD(rnGetBody:(nonnull NSString *)userId)

@end
