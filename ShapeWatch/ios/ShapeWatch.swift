import Foundation
import React
import UIKit
#if !arch(x86_64)
import CoreMotion
import QuantaCorp
#endif



class ScanController: UIViewController {
    var delegate: QCScanViewControllerDelegate?
    override func loadView() {
        view = UIView()
        view.backgroundColor = .orange
        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 200, height: 200))
        button.backgroundColor = .blue
        view.addSubview(button)
    }
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.scanWasCancelled()
    }
}

#if arch(x86_64)
protocol QCScanViewControllerDelegate: class {
    func scanWasCancelled()
}
#endif

@objc(ShapeWatch)
class ShapeWatch: RCTEventEmitter, QCScanViewControllerDelegate {
    func scanWasCancelled() {
        sendEvent(withName: "onDismiss", body: ["dismissed": true])   
    }
    
    func viewWillAppear(_ animated: Bool) {

    }
    
    func viewDidAppear(_ animated: Bool) {
        sendEvent(withName: "onAppear", body: nil)
    }
    
    func viewWillDisappear(_ animated: Bool) {

    }
    
    @objc func rnCreateToken() {
        #if !arch(x86_64)
        if (apiToken == nil) {
            let semaphore = DispatchSemaphore(value: 0)
            createApiToken(semaphore)
            let _ = semaphore.wait(timeout: .now() + 30)
        }
        #endif
    }
    
    @objc func rnGetBody(_ userId: NSString) {
        #if !arch(x86_64)
        if (body == nil) {
            let semaphore = DispatchSemaphore(value: 0)
            getBody(semaphore, userId: userId)
            let _ = semaphore.wait(timeout: .now() + 30)
        }
        #endif
    }
    
    #if !arch(x86_64)
    func viewDidDisappear(_ animated: Bool) {

    }
    var body: QCBody?
    let motionManager = CMMotionManager()
    var apiToken: QCApiToken?
    
    func createApiToken(_ semaphore: DispatchSemaphore) {
        QCAuthorizationApi.init().createApiToken(
            username: "shapewatch@quantacorp.io",
            password: "sw2017!",
            environment: .global,
            client: "Shapewatch-SDK",
            secret: "108e41a1-d5de-4d97-b293-3600ca8c79b6",
            resultHandler: {
                (token) in
                self.apiToken = token
                semaphore.signal()
            },
            errorHandler: {
                (error) in
                print("error creating token!\n\(error)")
            }
        )
    }
    


 
    
    func getBody(_ semaphore: DispatchSemaphore, userId: NSString) {
        QCPublicApi().getBody(
            alias: userId as String,
            projectId: 2,
            resultHandler: {
                (body) in
                self.body = body;
                semaphore.signal()
            },
            errorHandler: {
                (error) in
                print("GET Body Error \(error) \(userId)")
                semaphore.signal()
            },
            token: self.apiToken!
        )
    }

    func updateBody(
        _ semaphore: DispatchSemaphore,
        userId: NSString,
        weight: NSNumber,
        height: NSNumber,
        gender: NSNumber
    ) {
        if (self.body == nil) {
            var body = QCBody.init()
            body.alias = userId as String
            body.gender = gender == 1 ? .male : .female
            body.weight = weight as? UInt
            body.height = height as? UInt
            
            body.companyId = Int64(2)
            QCPublicApi().createBody(
                body: body,
                projectId: 2,
                resultHandler: {
                    (body) in
                    self.body = body;
                    self.updated = true
                    semaphore.signal()
            },
                errorHandler:{
                    (error) in
                    print("Could not create body for \(userId)")
                    semaphore.signal()
            },
                token: self.apiToken!)
        } else {
            self.body!.alias = userId as String
            self.body!.weight = weight as? UInt
            self.body!.height = height as? UInt
        
        QCPublicApi().updateBody(
            body: self.body!,
            resultHandler: {
                (body) in
                self.body = body
                self.updated = true
                semaphore.signal()
            },
            errorHandler: {
                (error) in
                print(error)
                semaphore.signal()
            },
            token: self.apiToken!
        )
        }
    }

    func getApiToken() -> QCApiToken? {
        return apiToken
    }
    
    func getCMMotionManager() -> CMMotionManager {
        return motionManager
    }
    
    func didUpdateAccelerometerReadings(accelerometerData: CMAccelerometerData?, error: Error?) {
        guard error == nil else { print("Accelerometer update failed: \(error!)"); return }
        guard accelerometerData != nil else { print("Accelerometer data missing"); return }
    }
    
    func scanDidSucceed(scanId: Int64, expectedProcessingTime: Int?) {
        sendEvent(withName: "onScan", body: ["scanId": scanId])
        print("Scan succeeded.\nScan with \(scanId)")
    }
    #endif

    var reactTag: NSNumber = 0.0
    var updated = false
    
    @objc func dismissPresentedViewController(_ reactTag: NSNumber) {
        DispatchQueue.main.async {
            if let view = SWBridge.sharedInstance.getBridge().uiManager.view(forReactTag: reactTag) {
                let presentedViewController: UIViewController! = view.reactViewController()
                presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
    
    }
    
    @objc func dismissScan() {
        print(self.reactTag.doubleValue)
        DispatchQueue.main.async {
            if let view = SWBridge.sharedInstance.getBridge().uiManager.view(forReactTag: self.reactTag) {
                let presentedViewController: UIViewController! = view.reactViewController()
                presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override static func requiresMainQueueSetup() -> Bool {
        return false
    }
    
    @objc func openViewController(_ reactTag: NSNumber,
                                  userId: NSString,
                                  weight: NSNumber,
                                  height: NSNumber,
                                  gender: NSNumber) {
        
        
        #if !arch(x86_64)
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        if (floatVersion >= 13) {
            DispatchQueue.main.async {
                if (!self.updated) {
                    let semaphore = DispatchSemaphore(value: 0)
                    self.updateBody(semaphore, userId: userId, weight: weight, height: height, gender: gender)
                    semaphore.wait()
                }
                self.updated = false
                let scanViewController = QCScanViewController(delegate: self as QCScanViewControllerDelegate)
                scanViewController.projectId = 2
                scanViewController.bodyId = self.body!.id
                scanViewController.height = height.uintValue
                scanViewController.modalPresentationStyle = .overFullScreen
                if let view = SWBridge.sharedInstance.getBridge().uiManager.view(forReactTag: reactTag) {
                    let presentedViewController = view.reactViewController()
                    presentedViewController?.present(scanViewController, animated: false, completion: nil)
                } else {
                    print("NO VIEW")
                }
            }
        } else {
            if (!self.updated) {
                let semaphore = DispatchSemaphore(value: 0)
                DispatchQueue.main.async {
                    self.updateBody(semaphore, userId: userId, weight: weight, height: height, gender: gender)
                }
                semaphore.wait()
            }
            self.updated = false
            let scanViewController = QCScanViewController(delegate: self as QCScanViewControllerDelegate)
            scanViewController.projectId = 2
            scanViewController.bodyId = self.body!.id
            scanViewController.height = height.uintValue
            scanViewController.modalPresentationStyle = .overFullScreen
            var presentedViewController: UIViewController? = nil
            let signal = DispatchSemaphore(value: 0)
            DispatchQueue.main.async {
                if let view = SWBridge.sharedInstance.getBridge().uiManager.view(forReactTag: reactTag) {
                    presentedViewController = view.reactViewController()
                    signal.signal()
                } else {
                    print("NO VIEW")
                }
            }
            signal.wait()
            if ((presentedViewController) != nil) {
                presentedViewController?.present(scanViewController, animated: false, completion: nil)
            }
        }
        #else
        DispatchQueue.main.async {
            if let view = SWBridge.sharedInstance.getBridge().uiManager.view(forReactTag: reactTag) {
                let presentedViewController: UIViewController! = view.reactViewController()
                let scanViewController = ScanController()
                scanViewController.view.backgroundColor = .red
                scanViewController.modalPresentationStyle = .overFullScreen
                (scanViewController as ScanController).delegate = self
                let button = UIButton(type: .system)
                button.frame = CGRect(x: 100, y: 100, width: 200, height: 200)
                button.backgroundColor = .blue
                button.setTitle("Dismiss", for: .normal)
                self.reactTag = reactTag
                button.addTarget(self, action: #selector(self.dismissScan), for: .touchUpInside)
                scanViewController.view.addSubview(button)
                presentedViewController.present(scanViewController, animated: true, completion: nil)
            }
        }
        #endif
    }
    
    override func supportedEvents() -> [String]! {
        return ["onScan", "onDismiss", "onAppear", "onScanError"]
    }
    
    #if !arch(x86_64)
    func scanDidFail(error: QCError?) -> Void {
        sendEvent(withName: "onScanError", body: ["error": "Scan failed: \(error!)"])
        print("Scan failed: \(error!)")
    }
    #endif
}
