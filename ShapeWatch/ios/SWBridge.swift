//
//  SWBridge.swift
//  swtestswift
//
//  Created by Alejandro Paredes Alva on 24/07/2019.
//  Copyright © 2019 GlobalScanning. All rights reserved.
//

import Foundation
import React

public class SWBridge: NSObject {
    public static let sharedInstance = SWBridge()
    var bridge: RCTBridge?
    
    func createBridgeIfNeeded() -> RCTBridge {
        if bridge == nil {
            bridge = RCTBridge.init(delegate: self, launchOptions: nil)
        }
        return bridge!
    }

    func getBridge() -> RCTBridge {
        return bridge!;
    }

    public func viewForModule(initialProperties: [String : String]?) -> RCTRootView {
        let viewBridge = createBridgeIfNeeded()
        let rootView: RCTRootView = RCTRootView(
            bridge: viewBridge,
            moduleName: "ShapeWatch",
            initialProperties: initialProperties)
        return rootView
    }
}

extension SWBridge: RCTBridgeDelegate {
    public func sourceURL(for bridge: RCTBridge!) -> URL! {
        return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
    }
}
