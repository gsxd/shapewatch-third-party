//
//  QuantaCorp.h
//  QuantaCorp SDK
//
//  Created by Thomas De Wilde on 30/07/2019.
//  Copyright © 2019 QuantaCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for QuantaCorp_SDK.
FOUNDATION_EXPORT double QuantaCorp_SDKVersionNumber;

//! Project version string for QuantaCorp_SDK.
FOUNDATION_EXPORT const unsigned char QuantaCorp_SDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QuantaCorp_SDK/PublicHeader.h>


