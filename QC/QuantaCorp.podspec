Pod::Spec.new do |spec|

  spec.name         = "QuantaCorp"
  spec.version      = "1.4.0"
  spec.summary      = "A short description of QC."
  spec.description  = <<-DESC
                  QuantaCorp Scan Library, used to take mobile scans
                   DESC

  spec.homepage     = "http://EXAMPLE/QC"
  spec.license      = "MIT"
  spec.source       = { :git => "https://bitbucket.org/gsxd/shapewatch-third-party.git", :tag => "#{spec.version}" }
  spec.author             = { "Alejandro Paredes Alva" => "a.alva@globalscanning.com" }
  
  spec.platform     = :ios, "11.0"
  spec.framework = "CoreMotion"
  spec.vendored_frameworks = "QuantaCorp.framework"

end
