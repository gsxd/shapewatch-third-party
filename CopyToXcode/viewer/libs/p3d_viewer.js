// commmonjs export
var isCommonJS = false;
if(typeof(module) !== 'undefined' && module.exports) {
    isCommonJS = true;
    module.exports = P3DViewerCreate;
}

function P3DViewerCreate(Module) {
    if (!Module) Module = {};

    Module.noInitialRun = true;

    if (!Module.preRun) Module.preRun = [];
    if (!Module.postRun) Module.postRun = [];
    if (!Module.print) Module.print = function(text) {console.log(text);};
    if (!Module.printErr) Module.printErr = function(text) {console.log(text);};

    var p3d = {
        noInputHandling: false,
        noRotate: false,
        noZoom: false,
        noPan: false,
        pendingTextureCallback: null,
        modelLoadProgressCallback: null,
        canvas: null,
        externalWebglContext: null,
        useMainLoop: true,
        useAntialiasing: true,
        LOG_VERBOSE: 1000,
        LOG_DEBUG: 600,
        LOG_INFO: 400,
        LOG_WARN: 300,
        LOG_ERROR: 200,
        LOG_FATAL: 100,
    };
    var oldMouseX = 0;
    var oldMouseY = 0;
    var oldTouchX = 0;
    var oldTouchY = 0;
    var touchZoomStartDist = 0;
    var mouseButton = 0;
    var screen =  {left: 0, top: 0, width: 1, height: 1};
    var modelLoaded = false;
    var gl = null;

    p3d.indexModes = {
      'POINTS': 0,
      'LINES': 1,
      'LINE_LOOP': 2,
      'LINE_STRIP': 3,
      'TRIANGLES': 4,
      'TRIANGLE_STRIP': 5,
      'TRIANGLE_FAN': 6
    };

    var getGltfDataType = function(typedArray) {
        if (typedArray instanceof Float32Array) {
            return 5126;
        }
        else if (typedArray instanceof Uint32Array) {
            return 5125;
        }
        else if (typedArray instanceof Uint16Array) {
            return 5123;
        }
        else if (typedArray instanceof Int16Array) {
            return 5122;
        }
        else if (typedArray instanceof Uint8Array) {
            return 5121;
        }
        else if (typedArray instanceof Int8Array) {
            return 5120;
        }
        return 0;
    };

    var moduleRunning = false;

    function mouseup( event ) {
        event.preventDefault();
        event.stopPropagation();
        document.removeEventListener( 'mousemove', mousemove );
        document.removeEventListener( 'mouseup', mouseup );
        Module._setIsNavigating(0);
    }

    function mousemove(event) {
        event.preventDefault();
        event.stopPropagation();

        var x = (event.clientX - screen.width * 0.5 - screen.left) / (screen.width * 0.5);
        var y = (screen.height * 0.5 + screen.top - event.clientY) / (screen.height * 0.5);

        if(mouseButton === 0 && !p3d.noRotate) {
            Module._rotateCam(x, y);
        } else if(mouseButton === 1 && !p3d.noZoom) {
            Module._zoomCam(1.0 + (y - oldMouseY));
        } else if(mouseButton === 2 && !p3d.noPan) {
            Module._panCam((x - oldMouseX) * (screen.width / screen.height) * 0.205, (oldMouseY - y) * 0.205);
        }

        oldMouseX = x;
        oldMouseY = y;
    }

    function mousedown(event) {
        if (modelLoaded) {
            refreshCanvasScreen(); /* need to refresh canvas boundaries here to ensure it works reliably within RN webviews */

            event.preventDefault();
            event.stopPropagation();

            var x = (event.clientX - screen.width * 0.5 - screen.left) / (screen.width * 0.5);
            var y = (screen.height * 0.5 + screen.top - event.clientY) / (screen.height * 0.5);
            oldMouseX = x;
            oldMouseY = y;
            mouseButton = event.button;

            if(event.button === 0) {
                Module._startRotateCam(x, y);
            }

            document.addEventListener( 'mousemove', mousemove, false );
            document.addEventListener( 'mouseup', mouseup, false );
            Module._setIsNavigating(1);
        }
    }

    function mousewheel(event) {
        event.preventDefault();
        event.stopPropagation();
        if (!p3d.noZoom) {
            Module._zoomCam(event.deltaY > 0 ? 1.2 : 1.0 / 1.2);
        }
    }

    function touchstart( event ) {

        if (modelLoaded) {
            refreshCanvasScreen(); /* need to refresh canvas boundaries here to ensure it works reliably within RN webviews*/

            var x = (event.touches[ 0 ].pageX - screen.width * 0.5 - screen.left) / (screen.width * 0.5);
            var y = (screen.height * 0.5 + screen.top - event.touches[ 0 ].pageY) / (screen.height * 0.5);
            oldTouchX = x;
            oldTouchY = y;

            switch ( event.touches.length ) {
                case 1:
                    Module._startRotateCam(x, y);
                    break;
                case 2:
                    var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
                    var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
                    touchZoomStartDist = Math.sqrt( dx * dx + dy * dy );
                    break;
            }

            document.addEventListener( 'touchmove', touchmove, { passive: false } );
            document.addEventListener( 'touchend', touchend, { passive: false } );
            Module._setIsNavigating(1);
        }
    }

    function touchmove( event ) {

        var x = (event.touches[ 0 ].pageX - screen.width * 0.5 - screen.left) / (screen.width * 0.5);
        var y = (screen.height * 0.5 + screen.top - event.touches[ 0 ].pageY) / (screen.height * 0.5);

        if(event.button === 0 && !p3d.noRotate) {
            Module._rotateCam(x, y);
        } else if(event.button === 1 && !p3d.noZoom) {
            Module._zoomCam(1.0 + (y - oldTouchY));
        } else if(event.button === 2 && !p3d.noPan) {
            Module._panCam((x - oldTouchX) * (screen.width / screen.height) * 0.205, (oldTouchY - y) * 0.205);
        }


        event.preventDefault();
        event.stopPropagation();

        switch ( event.touches.length ) {
            case 1:
                if (!p3d.noRotate) {
                    Module._rotateCam(x, y);
                }
                break;

            case 2:
                if (!p3d.noZoom) {
                    var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
                    var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
                    var distEnd = Math.sqrt( dx * dx + dy * dy );
                    var factor = touchZoomStartDist / distEnd;
                    touchZoomStartDist = distEnd;
                    Module._zoomCam(factor);
                }
                if (!p3d.noPan) {
                    Module._panCam((x - oldTouchX) * (screen.width / screen.height) * 0.205, (oldTouchY - y) * 0.205);
                }
                break;

            case 3:
                if (!p3d.noPan) {
                    Module._panCam((x - oldTouchX) * (screen.width / screen.height) * 0.205, (oldTouchY - y) * 0.205);
                }
                break;
        }

        oldTouchX = x;
        oldTouchY = y;
    }

    function touchend( event ) {
        document.removeEventListener( 'touchmove', touchmove );
        document.removeEventListener( 'touchend', touchend );
        Module._setIsNavigating(0);
    }

    function loadModel(meshId, data, extension) {
        var size = data.byteLength;
        Module.print("loaded bytes: " + size);
        var buf = Module._malloc(size);
        Module.HEAPU8.set(new Uint8Array(data), buf);
        Module.ccall('loadModel', 'void', ['number', 'number', 'number', 'string'], [meshId, buf, size, extension]);
        Module._free(buf);
        modelLoaded = true;
    }

    function loadMorphTargetModel(meshId, time, data, extension) {
        var size = data.byteLength;
        Module.print("loaded bytes: " + size);
        var buf = Module._malloc(size);
        Module.HEAPU8.set(new Uint8Array(data), buf);
        Module.ccall('loadMorphTargetModel', 'void', ['number', 'number', 'number', 'number', 'string'], [meshId, time, buf, size, extension]);
        Module._free(buf);
    }

    function refreshCanvasScreen() {
        screen = {left:Module.canvas.offsetLeft, top:Module.canvas.offsetTop, width:Math.max(1, Module.canvas.offsetWidth), height:Math.max(1, Module.canvas.offsetHeight)};
    }

    function postRun() {
        moduleRunning = true;

        var OBJECT = 0;
        var MESH = 1;
        var _idObjCache = [[],[]];

        var idToTypeobj = function(type, id) {
            var cached = _idObjCache[type][id];
            return (typeof cached !== 'undefined') ? cached : (_idObjCache[type][id] = {type:type, id:id});
        }

        /* Object and mesh generators */
        var addObject = Module.cwrap('addObject', 'number', ['number']);
        p3d.addObject = function(meshId) {
            return idToTypeobj(OBJECT, addObject((typeof meshId !== 'undefined') ? _getMesh(meshId, 'addObject') : 0));
        };
        var addMesh = Module.cwrap('addMesh', 'number', []);
        p3d.addMesh = function() {
            return idToTypeobj(MESH, addMesh());
        };
        var getObjectMesh = Module.cwrap('getObjectMesh', 'int', ['number']);
        p3d.getObjectMesh = function(object) {
            var _mesh = getObjectMesh(_getObject(object, 'getObjectMesh'));
            return (_mesh) ? idToTypeobj(MESH, _mesh) : 0;
        };

        /*
        *  Filters
        */
        // Extract object id from addObject pointer
        var _getObject = function(o, _func) {
            if (typeof o === 'object' && o.type === OBJECT) {
                return o.id;
            }
            console.error("Call to " + _func + " with invalid object", o);
            return 0;
        };
        // Extract mesh id from addMesh or addObject pointer, latter using the assigned mesh if any
        var _getMesh = function(o, _func) {
            if (typeof o === 'object') {
                if (o.type === MESH) {
                    return o.id;
                }
                if (o.type === OBJECT) {
                    return getObjectMesh(o.id);
                }
            }
            console.error("Call to " + _func + " with invalid mesh", o);
            return 0;
        };
        var _boolean = function(v) {
            return v ? 1 : 0;
        }


        // Custon Module.cwrap implementation that acceps additional argument types
        //  boolean = gets converted to 1 if true, 0 if false
        //  p3dobject = p3dobject id returned by addObject()
        //  p3dmesh = p3dmesh id returned by addMesh(), or p3dobject that has a mesh assigned to it
        var p3dcwrap = function(func, retval, args) {

            // generate custom filters
            var customFilters = [];
            for (var i=0; i<args.length; i++) {
                if (args[i] === "boolean") {
                    args[i] = "number";
                    customFilters[i] = _boolean;
                }
                else if (args[i] === "p3dobject") {
                    args[i] = "number";
                    customFilters[i] = _getObject;
                }
                else if (args[i] === "p3dmesh") {
                    args[i] = "number";
                    customFilters[i] = _getMesh;
                }
            }

            var cwrap = Module.cwrap(func, retval, args);
            return function() {
                var args = Array.prototype.slice.call(arguments);
                customFilters.forEach(function(filter, index) {
                    var orig_arg = args[index];
                    args[index] = filter(args[index], func);
                });
                return cwrap.apply(null, args);
            };
        };

        p3d.setLogLevel = Module.cwrap('setLogLevel', 'void', ['number']);
        p3d.setUrlPrefix = Module.cwrap('setUrlPrefix', 'void', ['string']);

        p3d.loadBaseModelAsMorphTarget = p3dcwrap('loadBaseModelAsMorphTarget', 'void', ['p3dmesh', 'number']);
        p3d.releaseMorphTarget = p3dcwrap('releaseMorphTarget', 'void', ['p3dmesh', 'number']);
        p3d.releaseMorphTargets = p3dcwrap('releaseMorphTargets', 'void', ['p3dmesh']);
        var setMaterialProperty = p3dcwrap('setMaterialProperty', 'void', ['p3dobject', 'number', 'string', 'string']);
        p3d.setMaterialProperty = function(object, materialIndex, property, value) {
            setMaterialProperty(object, materialIndex, property, (typeof value === 'string' || value instanceof String) ? value : String(value));
        };
        var setMaterialTexture = p3dcwrap('setMaterialTexture', 'void', ['p3dobject', 'number', 'string', 'string', 'boolean', 'number', 'number', 'number']);
        p3d.setMaterialTexture = function(object, materialIndex, type, url, has_alpha, mapping, encoding, gamma) {
            mapping = (mapping === 'equirect') ? 1 : 0;
            encoding = (encoding === 'rgbe') ? 1 : 0;
            gamma = (gamma > 0.0) ? gamma : 0.0;
            setMaterialTexture(object, materialIndex, type, url, has_alpha, mapping, encoding, gamma);
        };
        var setGlobalProperty = p3dcwrap('setGlobalProperty', 'void', ['string', 'string']);
        p3d.setGlobalProperty = function(property, value) {
            setGlobalProperty(property, (typeof value === 'string' || value instanceof String) ? value : String(value));
        };
        var setGlobalTexture = p3dcwrap('setGlobalTexture', 'void', ['string', 'string', 'boolean', 'number', 'number', 'number']);
        p3d.setGlobalTexture = function(type, url, has_alpha, mapping, encoding, gamma) {
            mapping = (mapping === 'equirect') ? 1 : 0;
            encoding = (encoding === 'rgbe') ? 1 : 0;
            gamma = (gamma > 0.0) ? gamma : 0.0;    // 0.0 = default
            setGlobalTexture(type, url, has_alpha, mapping, encoding, gamma);
        };
        p3d.setMaterialVisible = p3dcwrap('setMaterialVisible', 'void', ['p3dobject', 'number', 'boolean']);
        p3d.setObjectVisible = p3dcwrap('setObjectVisible', 'void', ['p3dobject', 'boolean']);
        p3d.setMaterialHighlight = p3dcwrap('setMaterialHighlight', 'void', ['p3dobject', 'number', 'number']);
        p3d.setLinesEffect = p3dcwrap('setLinesEffect', 'void', ['p3dobject', 'number', 'number']);
        p3d.setSceneStaticForce = p3dcwrap('setSceneStaticForce', 'void', ['number', 'number', 'number']);
        p3d.setUseComposer = p3dcwrap('setUseComposer', 'void', ['bool']);
        p3d.setBloomEffect = p3dcwrap('setBloomEffect', 'void', ['number', 'number', 'number']);
        p3d.setRenderFactor = p3dcwrap('setRenderFactor', 'void', ['number', 'boolean']);
        p3d.setEnforceRenderFactor = p3dcwrap('setEnforceRenderFactor', 'void', ['number']);
        p3d.setLightColor = p3dcwrap('setLightColor', 'void', ['number', 'number', 'number', 'number']);
        p3d.setLightDirection = p3dcwrap('setLightDirection', 'void', ['number', 'number', 'number', 'number']);
        var resize_gl = p3dcwrap('resize_gl', 'void', ['number', 'number']);
        p3d.resize_gl = function(width, height) {
            resize_gl(width, height);
            refreshCanvasScreen();
        };
        p3d.setNavMode = p3dcwrap('setNavMode', 'void', ['string']);
        p3d.setShadingMode = p3dcwrap('setShadingMode', 'void', ['string']);
        p3d.setCameraPosition = p3dcwrap('setCameraPosition', 'void', ['number', 'number', 'number', 'boolean', 'boolean']);
        p3d.setCameraUp = p3dcwrap('setCameraUp', 'void', ['number', 'number', 'number', 'boolean', 'boolean']);
        p3d.setCameraTarget = p3dcwrap('setCameraTarget', 'void', ['number', 'number', 'number', 'boolean', 'boolean']);
        p3d.setCameraFov = p3dcwrap('setCameraFov', 'void', ['number', 'number']);
        p3d.setCameraProjectionType = p3dcwrap('setCameraProjectionType', 'void', ['string']);
        var getCameraPosition = p3dcwrap('getCameraPosition', 'string', []);
        p3d.getCameraPosition = function() {
            var str = getCameraPosition();
            var c = str.split(' ');
            return {x: parseFloat(c[0]), y: parseFloat(c[1]), z: parseFloat(c[2])};
        };
        var getCameraUp = p3dcwrap('getCameraUp', 'string', []);
        p3d.getCameraUp = function() {
            var str = getCameraUp();
            var c = str.split(' ');
            return {x: parseFloat(c[0]), y: parseFloat(c[1]), z: parseFloat(c[2])};
        };
        var getCameraTarget = p3dcwrap('getCameraTarget', 'string', []);
        p3d.getCameraTarget = function() {
            var str = getCameraTarget();
            var c = str.split(' ');
            return {x: parseFloat(c[0]), y: parseFloat(c[1]), z: parseFloat(c[2])};
        };
        var setSpin = p3dcwrap('setSpin', 'void', ['number', 'number', 'number']);
        p3d.setSpin = function(rad, updateView, updateDefault) {
            setSpin(rad, updateView ? 1 : 0, updateDefault ? 1 : 0);
        };
        p3d.getSpin = p3dcwrap('getSpin', 'number', []);
        p3d.setAutoSpin = p3dcwrap('setAutoSpin', 'void', ['number']);
        p3d.setLazyloadShadingModes = p3dcwrap('setLazyloadShadingModes', 'void', ['boolean']);
        p3d.setLazyloadTextures = p3dcwrap('setLazyloadTextures', 'void', ['number']);
        p3d.setUseMultimaterial = p3dcwrap('setUseMultimaterial', 'void', ['p3dobject', 'boolean']);
        // Enable use of morph targets (default is disabled)
        // Needs to be set before assigning any buffers to the mesh so that engine can save necessary
        // cpu side copies for morph target generation
        // setUseMorphTargets(p3dmesh, value, type)
        // value: boolean indicating whether morph targets are used (increases memory usage)
        // type: optional string indicating type of the morph targets.  "time" = timeline based full mesh morph targets
        //                                                              "gltf" = gltf style shapekey like morph targets (default)
        p3d.setUseMorphTargets = p3dcwrap('setUseMorphTargets', 'void', ['p3dmesh', 'boolean', 'string']);
        p3d.setUseTangents = p3dcwrap('setUseTangents', 'void', ['p3dmesh', 'boolean']);
        p3d.setUseUVs = p3dcwrap('setUseUVs', 'void', ['p3dmesh', 'boolean']);
        p3d.setAutoCenter = p3dcwrap('setAutoCenter', 'void', ['p3dmesh', 'boolean']);
        var setMorphPlayback = p3dcwrap('setMorphPlayback', 'void', ['p3dobject', 'number', 'number']);
        p3d.setMorphPlayback = function(objectId, speed, stopTime) {
            setMorphPlayback(objectId, speed, (typeof stopTime !== 'undefined') ? stopTime : 999999.0);
        };
        p3d.setMorphPlaybackLoop = p3dcwrap('setMorphPlaybackLoop', 'void', ['p3dobject', 'boolean']);
        p3d.setMorphBetween = p3dcwrap('setMorphBetween', 'void', ['p3dobject', 'number', 'number']);
        p3d.setMorphTime = p3dcwrap('setMorphTime', 'void', ['p3dobject', 'number']);
        p3d.getMorphTime = p3dcwrap('getMorphTime', 'number', ['p3dobject']);
        p3d.setModelOrientation = p3dcwrap('setModelOrientation', 'void', ['p3dobject', 'number', 'number', 'number']);
        p3d.render = p3dcwrap('render', 'void', []);
        // Use mesh id as argument to assign to the newly created object
        p3d.deleteObject = p3dcwrap('deleteObject', 'void', ['p3dobject']);
        p3d.setObjectParent = p3dcwrap('setObjectParent', 'void', ['p3dobject', 'p3dobject']);
        p3d.clearObjectChildren = p3dcwrap('clearObjectChildren', 'void', ['p3dobject']);
        p3d.setObjectLocation = p3dcwrap('setObjectLocation', 'void', ['p3dobject', 'number', 'number', 'number']);
        var setObjectRotationEuler = p3dcwrap('setObjectRotationEuler', 'void', ['p3dobject', 'number', 'number', 'number']);
        var setObjectRotationQuat = p3dcwrap('setObjectRotationQuat', 'void', ['p3dobject', 'number', 'number', 'number', 'number']);
        p3d.setObjectRotation = function(objectId, x, y, z, w) {
            if (typeof w !== 'undefined') {
                setObjectRotationQuat(objectId, x, y, z, w);
            }
            else {
                setObjectRotationEuler(objectId, x, y, z);
            }
        }
        p3d.setObjectScale = p3dcwrap('setObjectScale', 'void', ['p3dobject', 'number', 'number', 'number']);
        // Factor for child element locations. This can be useful for skaling lengths of hierarchial structures
        // like skeletons wihout affecting sizes of any of the objects
        p3d.setObjectChildScaleFactor = p3dcwrap('setObjectChildScaleFactor', 'void', ['p3dobject', 'number']);
        // Determines whether object delegates its scale to its children. This is also is useful for
        // skeletons where you need to increase size of one bone but leave all bones higher in the
        // hierarchy unaffected
        p3d.setObjectDelegateScale = p3dcwrap('setObjectDelegateScale', 'void', ['p3dobject', 'boolean']);
        // Set object to be considered the scene "root". Only descending objects will be rendered
        p3d.setSceneObject = p3dcwrap('setSceneObject', 'void', ['p3dobject']);
        p3d.setSkinningAttached = p3dcwrap('setSkinningAttached', 'void', ['p3dobject', 'boolean']);
        // Set gltf compatible skinning inverse bind matrices and joint object ids
        // inverseBindMatrices: (joint count)*16 Float32Array of floating point matrices
        // joints: Uint16Array of object ids corresponding to skeleton bones
        p3d.setMeshSkinningJoints = function(meshId, inverseBindMatrices, joints) {
            meshId = _getMesh(meshId, 'setMeshSkinningJoints');

            var count = joints.length;
            if (inverseBindMatrices.length !== count * 16) {
                console.error("setMeshSkinningJoints(): inverseBindMatrices and joints count doesn't match");
                return;
            }
            var size = count * 16 * inverseBindMatrices.BYTES_PER_ELEMENT;
            var ibbuff = Module._malloc(size);
            Module.HEAPU8.set(new Uint8Array(inverseBindMatrices.buffer, inverseBindMatrices.byteOffset, size), ibbuff);
            size = count * joints.BYTES_PER_ELEMENT;
            var jointbuff = Module._malloc(size);
            Module.HEAPU8.set(new Uint8Array(joints.buffer, joints.byteOffset, size), jointbuff);
            Module._setMeshSkinningJoints(meshId, count, ibbuff, jointbuff);
            Module._free(ibbuff);
            Module._free(jointbuff);
        }
        // Set gltf morph target weights
        // weights = Float32Array of weights
        p3d.setMeshMorphTargetWeights = function(meshId, weights) {
            meshId = _getMesh(meshId, 'setMeshSkinningJoints');

            if (!(weights instanceof Float32Array)) {
                weights = new Float32Array(weights);
            }
            var count = weights.length;
            var size = count * weights.BYTES_PER_ELEMENT;
            var buff = Module._malloc(size);
            Module.HEAPU8.set(new Uint8Array(weights.buffer, weights.byteOffset, size), buff);
            Module._setMeshMorphTargetWeights(meshId, count, buff);
            Module._free(buff);
        }
        p3d.setMeshMorphTargetWeight = p3dcwrap('setMeshMorphTargetWeight', 'void', ['p3dmesh', 'number', 'number']);
        p3d.deleteMesh = p3dcwrap('deleteMesh', 'void', ['p3dmesh']);
        p3d.setObjectMesh = p3dcwrap('setObjectMesh', 'void', ['p3dobject', 'p3dmesh']);
        // Directly set mesh index buffer
        // type:        INDEX, or WIRE_INDEX for a custom GL_LINES buffer
        // chunkIndex:  Index in case of multi chunk/primitive mesh
        // data:        A typed array containing the actual buffer
        // mode:        A gltf compatible primitive mode value. Use p3d.indexModes for supported values
        // material:    Material index of the this mesh chunk
        p3d.setMeshIndexBuffer = function(meshId, type, chunkIndex, data, mode, material) {
            meshId = _getMesh(meshId, 'setMeshIndexBuffer');
            var datatype = getGltfDataType(data);

            if (!datatype) {
                console.error("setMeshIndexBuffer(): invalid data type");
                return;
            }

            var count = data.length;
            var typesize = data.BYTES_PER_ELEMENT;
            var size = count * typesize;
            var buf = Module._malloc(size);
            Module.HEAPU8.set(new Uint8Array(data.buffer, data.byteOffset, size), buf);
            Module.ccall('setMeshIndexBuffer', 'void', ['number', 'string', 'number', 'number', 'number', 'number', 'number', 'number', 'number'],
                                                       [meshId, type, chunkIndex, buf, count, typesize, datatype, mode || 0, material || 0]);
            Module._free(buf);
			// Enable navigation listeners when first index buffer is set
			modelLoaded = true;
        }
        var setMeshAttributeBuffer = function(meshId, type, chunkIndex, data, components, normalized, stride, isMorphTarget, time) {
            meshId = _getMesh(meshId, 'setMeshAttributeBuffer');
            var datatype = getGltfDataType(data);

            if (!datatype) {
                console.error("setMeshAttributeBuffer(): invalid data type");
                return;
            }
            if (!data.length || (data.length % components)) {
                console.error("setMeshAttributeBuffer(): data length doesn't match given components");
                return;
            }

            var count = data.length / components;
            var typesize = data.BYTES_PER_ELEMENT;
            var size = count * components * typesize;
            var buf = Module._malloc(size);
            Module.HEAPU8.set(new Uint8Array(data.buffer, data.byteOffset, size), buf);
            Module.ccall('setMeshAttributeBuffer', 'void', ['number', 'string', 'number', 'number', 'number', 'number', 'number', 'number', 'number', 'number', 'number', 'number'],
                                                       [meshId, type, chunkIndex, buf, count, components, typesize, datatype, normalized ? 1 : 0, stride, isMorphTarget ? 1 : 0, time]);
            Module._free(buf);
        }
        // Directly set mesh attribute buffer
        // type:        A gltf compatible buffer target value. Supported values are "POSITION", "NORMAL", "TANGENT", "TEXCOORD_0", "JOINTS_0", "WEIGHTS_0"
        // chunkIndex:  Index in case of multi chunk/primitive mesh
        // data:        A typed array containing the actual buffer
        // components:  Number of components per vector (1,2,3,4)
        // normalized:  glVertexAttribPointer, Whether values should be normalized when converted to floats for the GPU
        // stride:      glVertexAttribPointer, byte offset between buffer elements
        p3d.setMeshAttributeBuffer = function(meshId, type, chunkIndex, data, components, normalized, stride) {
            setMeshAttributeBuffer(meshId, type, chunkIndex, data, components, normalized, stride, false, 0);
        }
        // Same for morph target buffers. Note that each morp target "time" buffer is expected to have same number of chunks and vertices than the base mesh buffers
        p3d.setMorphTargetAttributeBuffer = function(meshId, time, type, chunkIndex, data, components, normalized, stride) {
            setMeshAttributeBuffer(meshId, type, chunkIndex, data, components, normalized, stride, true, time);
        }
        p3d.updateSceneScale = p3dcwrap('updateSceneScale', 'void', []);
        p3d.clearTextureCache = p3dcwrap('clearTextureCache', 'void', []);
        p3d.getObjectPendingTextures = p3dcwrap('getObjectPendingTextures', 'number', ['p3dobject', 'boolean']);
        p3d.applyDefaultCamera = p3dcwrap('applyDefaultCamera', 'void', []);
        p3d.setResetDuration = p3dcwrap('setResetDuration', 'void', ['number']);
        p3d.setCameraRotationLimits = p3dcwrap('setCameraRotationLimits', 'void', ['number', 'number']);
        p3d.setCameraZoomLimits = p3dcwrap('setCameraZoomLimits', 'void', ['number', 'number']);
        p3d.resetCamera = p3dcwrap('resetCam', 'void', ['boolean']);
        p3d.materialCount = p3dcwrap('materialCount', 'number', ['p3dmesh']);
        p3d.vertexCount = p3dcwrap('vertexCount', 'number', ['p3dmesh']);
        p3d.polygonCount = p3dcwrap('polygonCount', 'number', ['p3dmesh']);
        p3d.boundingRadius = p3dcwrap('boundingRadius', 'number', ['p3dmesh']);
        p3d.skyStatus = p3dcwrap('skyStatus', 'number', []);
        var pickPixel = p3dcwrap('pickPixel', 'string', ['number', 'number', 'number']);
        p3d.pickPixel = function(x, y, extra) {
            var str = pickPixel(x, y, extra ? 1 : 0);
            if (!str.length) {
                return false;
            }
            var c = str.split(' ');
            var data = {
                    materialIndex: parseInt(c[0]),
                    object: idToTypeobj(OBJECT, parseInt(c[1])),
                    x: parseFloat(c[2]), y: parseFloat(c[3]), z: parseFloat(c[4]),
                    wx: parseFloat(c[5]), wy: parseFloat(c[6]), wz: parseFloat(c[7])};
            if (extra) {
                data.nx = parseFloat(c[8]);
                data.ny = parseFloat(c[9]);
                data.nz = parseFloat(c[10]);
            }
            return data;
        };
        var adjustOrientation = p3dcwrap('adjustOrientation', 'string', ['p3dobject', 'number', 'number', 'number', 'number']);
        p3d.adjustOrientation = function(objectId, axisX, axisY, axisZ, angle) {
            var str = adjustOrientation(objectId, axisX, axisY, axisZ, angle);
            var c = str.split(' ');
            return {x: parseFloat(c[0]), y: parseFloat(c[1]), z: parseFloat(c[2])};
        };
        p3d.gyroOrientation = p3dcwrap('gyroOrientation', 'void', ['number', 'number', 'number', 'number']);
        p3d.exitViewer = p3dcwrap('exitViewer', 'void', []);


        var mat4ToHeap = function(mat) {
            var data = new Float32Array( 16 );
            for (var i=0; i<16; i++) {
                data[i] = mat[i];
            }

            // Get data byte size, allocate memory on Emscripten heap, and get pointer
            var nDataBytes = data.length * data.BYTES_PER_ELEMENT;
            var dataPtr = Module._malloc(nDataBytes);
            // Copy data to Emscripten heap (directly accessed from Module.HEAPU8)
            var dataHeap = new Uint8Array(Module.HEAPU8.buffer, dataPtr, nDataBytes);
            dataHeap.set(new Uint8Array(data.buffer));

            return dataPtr;
        }

        p3d.animationStep = p3dcwrap('animationStep', 'void', ['number']);
        p3d.setClearColor = p3dcwrap('setClearColor', 'void', ['number', 'number', 'number', 'number']);
        // Render composer rendertargets if used
        p3d.renderComposer = p3dcwrap('renderComposer', 'void', []);
        // Render finished scene to framebuffer
        p3d.renderToFramebuffer = p3dcwrap('renderToFramebuffer', 'void', ['boolean']);

        var renderObjectPass = p3dcwrap('renderObjectPass', 'void', ['p3dobject', 'number', 'number', 'number', 'number']);
        p3d.renderObjectPass = function(emModel, pass, proj, view, model){
            var projDat, viewDat, modelDat;

            projDat = (proj) ? mat4ToHeap(proj) : null;
            viewDat = (view) ? mat4ToHeap(view) : null;
            modelDat = (model) ? mat4ToHeap(model) : null;

            // call the emscripten function
            renderObjectPass(emModel, pass, projDat, viewDat, modelDat);

            if (projDat) {
                Module._free(projDat);
            }
            if (viewDat) {
                Module._free(viewDat);
            }
            if (modelDat) {
                Module._free(modelDat);
            }
        }

        p3d.renderObject = function(emModel, proj, view, model){
            p3d.renderObjectPass(emModel, -1, proj, view, model);
        }

        var renderSky = p3dcwrap('renderSky', 'void', ['number', 'number']);
        p3d.renderSky = function(proj, view){
            var projDat, viewDat;

            projDat = (proj) ? mat4ToHeap(proj) : null;
            viewDat = (view) ? mat4ToHeap(view) : null;

            // call the emscripten function
            renderSky(projDat, viewDat);

            if (projDat) {
                Module._free(projDat);
            }
            if (viewDat) {
                Module._free(viewDat);
            }
        }

        var overrideCamera = p3dcwrap('overrideCamera', 'void', ['number', 'number']);
        p3d.overrideCamera = function(proj, view){
            var projDat, viewDat;

            projDat = (proj) ? mat4ToHeap(proj) : null;
            viewDat = (view) ? mat4ToHeap(view) : null;

            // call the emscripten function
            overrideCamera(projDat, viewDat);

            if (projDat) {
                Module._free(projDat);
            }
            if (viewDat) {
                Module._free(viewDat);
            }
        }

        var overrideObjectWorldMatrix = p3dcwrap('overrideObjectWorldMatrix', 'void', ['p3dobject', 'number']);
        p3d.overrideObjectWorldMatrix = function(obj, transform){
            var matDat;

            matDat = (transform) ? mat4ToHeap(transform) : null;

            // call the emscripten function
            overrideObjectWorldMatrix(obj, matDat);

            if (matDat) {
                Module._free(matDat);
            }
        }

        var setObjectMatrix = p3dcwrap('setObjectMatrix', 'void', ['p3dobject', 'number']);
        p3d.setObjectMatrix = function(obj, transform){
            var matDat;

            matDat = (transform) ? mat4ToHeap(transform) : null;

            // call the emscripten function
            setObjectMatrix(obj, matDat);

            if (matDat) {
                Module._free(matDat);
            }
        }
        // Create empty texture that can be updated with custom data
        // r,g,b,a = color the texture is initialized with
        var addEmptyTexture = p3dcwrap('addEmptyTexture', 'void', ['string', 'number', 'number', 'number', 'number']);
        var _emptyId = 0;
        p3d.addEmptyTexture = function(r,g,b,a) {
            var id = '//custom:' + _emptyId++;
            addEmptyTexture(id, r || 0.0, g || 0.0, b || 0.0, a || 0.0);
            return id;
          }

        var _getWebGLTexture = p3dcwrap('getWebGLTexture', 'void', ['string']);
        var getTextureWebgl = function(id) {
            P3DViewer.Module['_webglTexture'] = null;
            _getWebGLTexture(id);
            return P3DViewer.Module['_webglTexture'];
          }
        // Update texture pixels from a HTML element (HTMLImageElement, HTMLCanvasElement, HTMLVideoElement)
        // Texture must be created with addEmptyTexture()
        p3d.updateTextureFromHtmlElement = function(texture, element) {
            var webglTex = getTextureWebgl(texture);
            if (webglTex) {
                var level = 0;
                var internalFormat = gl.RGBA;
                var srcFormat = gl.RGBA;
                var srcType = gl.UNSIGNED_BYTE;
                gl.bindTexture(gl.TEXTURE_2D, webglTex);
                gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                                srcFormat, srcType, element);
            }
            else {
                console.error("Called updateTextureFromHtmlElement() with invalid texture id");
            }
        }

        p3d.loadBin = function(meshId, binUrl, callback) {
            meshId = _getMesh(meshId, 'loadBin');
            if(moduleRunning) {
                loadBin(meshId, binUrl, callback);
            } else {
                Module.postRun.push(loadBin.bind(this, meshId, binUrl, callback));
            }
        }

        p3d.loadMorphTargetBin = function(meshId, time, binUrl, callback) {
            meshId = _getMesh(meshId, 'loadMorphTargetBin');
            if(moduleRunning) {
                loadMorphTargetBin(meshId, time, binUrl, callback);
            } else {
                Module.postRun.push(loadMorphTargetBin.bind(this, meshId, time, binUrl, callback));
            }
        }

        p3d.loadBinFromMemory = function(meshId, data, callback) {
            meshId = _getMesh(meshId, 'loadBinFromMemory');
            loadModel(meshId, data, ".bin");
            if (callback) {
                callback();
            }
        }

         p3d.loadMorphTargetBinFromMemory = function(meshId, time, data, callback) {
            meshId = _getMesh(meshId, 'loadMorphTargetBinFromMemory');
            loadMorphTargetModel(meshId, time, data, ".bin");
            if (callback) {
                callback();
            }
        }

        p3d.loadBlend = function(meshId, blendUrl, callback) {
            meshId = _getMesh(meshId, 'loadBlend');
            if(moduleRunning) {
                loadBlend(meshId, blendUrl, callback);
            } else {
                Module.postRun.push(loadBlend.bind(this, meshId, blendUrl, callback));
            }
        }

        p3d.registerShaderMaterial = p3dcwrap('registerShaderMaterial', 'void', ['string', 'string', 'string']);
        p3d.registerShaderUniform = p3dcwrap('registerShaderUniform', 'void', ['string', 'string', 'string', 'string', 'string']);
        p3d.registerShaderTexture = p3dcwrap('registerShaderTexture', 'void', ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string']);
        p3d.registerShaderChunk = p3dcwrap('registerShaderChunk', 'void', ['string', 'string']);
        p3d.registerShaderFile = p3dcwrap('registerShaderFile', 'void', ['string', 'string']);



        function handleFileSelect(meshId, evt) {
            var file = evt.target.files[0];
            console.log("loading:", file.name);
            var extension = /\.[^.]+$/.exec(file.name);
            if(!extension) {
                console.log(" filename doesn't have extension");
                return;
            }
            extension = extension[0];
            console.log(" extension:", extension);
            var reader = new FileReader();
            reader.onload = function(e) {
                meshId = _getMesh(meshId, 'handleFileSelect');
                console.log('handleFileSelect', meshId);
                loadModel(meshId, reader.result, extension);
            }
            reader.readAsArrayBuffer(file);
        }


        var fileElem = document.getElementById('modelFile');
        if(fileElem) {
            var p3dObject = null;
            var p3dMesh = null;
            fileElem.addEventListener('change', function(evt) {
                if(!p3dObject) {
                    p3dMesh = p3d.addMesh();
                    p3dObject = p3d.addObject(p3dMesh);
                }
                handleFileSelect(p3dMesh, evt);
            }, false);
        }

        if (!p3d.noInputHandling) {
            // setup mouse handling
            var canvas = Module.canvas;
            refreshCanvasScreen();
            canvas.addEventListener( 'contextmenu', function (event) {event.preventDefault();}, false );
            canvas.addEventListener( 'mousedown', mousedown, false );
            canvas.addEventListener( 'touchstart', touchstart, { passive: false } );
            canvas.addEventListener( 'wheel', mousewheel, false );
        }
    };

    function loadBin(meshId, binUrl, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', binUrl, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function(e) {
            if (this.readyState === 4 && (this.status === 200 || this.status === 0)) {
                loadModel(meshId, this.response, ".bin");
                if (callback) {
                    callback();
                }
            }
        };
        xhr.onprogress = function(event) {
            if (p3d.modelLoadProgressCallback) {
                p3d.modelLoadProgressCallback(event);
            }
        };
        xhr.send();
    }

    function loadMorphTargetBin(meshId, time, binUrl, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', binUrl, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function(e) {
            if (this.readyState === 4 && (this.status === 200 || this.status === 0)) {
                loadMorphTargetModel(meshId, time, this.response, ".bin");
                if (callback) {
                    callback();
                }
            }
        };
        xhr.send();
    }

    function loadBlend(meshId, blendUrl, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', blendUrl, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function(e) {
            if (this.readyState === 4 && (this.status === 200 || this.status === 0)) {
                p3d.setUrlPrefix(blendUrl.substr(0, blendUrl.lastIndexOf('/') + 1));
                loadModel(meshId, this.response, ".blend");
                if (callback) {
                    callback();
                }
            }
        };
        xhr.onprogress = function(event) {
            if (p3d.modelLoadProgressCallback) {
                p3d.modelLoadProgressCallback(event);
            }
        };
        xhr.send();
    }

    Module.TOTAL_MEMORY = 16 * 1024 * 1024;

    p3d.postRun = postRun;
    p3d.initialize = function() {
        if (!p3d.canvas) {
            logger.error('You must set canvas before initializing viewer');
            return;
        }

        
        /* call emscripten main (latest emscripten versions expect this to be called before any exported functions) */
        Module.callMain();

        /* init module settings */
        Module['canvas'] = p3d.canvas;
        if (p3d.externalWebglContext) {
            Module['externalContext'] = p3d.externalWebglContext;
            Module.ccall('setUseExternalContext', 'void', ['number'], [p3d.externalWebglContext ? 1 : 0]);
        }
        Module.ccall('setUseMainLoop', 'void', ['number'], [p3d.useMainLoop ? 1 : 0]);
        Module.ccall('setUseAntialiasing', 'void', ['number'], [p3d.useAntialiasing ? 1 : 0]);

        /* initialize viewer */
        Module._initViewer();

        gl = p3d.canvas.getContext('webgl') || p3d.canvas.getContext('experimental-webgl');

        /* init control functions */
        p3d.postRun();
    }

    Module.P3DViewer = p3d;
    p3d.Module = Module;
    if (isCommonJS) {
        require('./p3d-em.js')(Module);
    } else {
        P3DEmscriptenModule(Module);
    }
    return p3d;
};
