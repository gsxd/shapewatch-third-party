### Clone the repo

`git clone https://bitbucket.org/gsxd/shapewatch-third-party.git`

### Add pods to your project

Be sure to fix `shapepods` path to the location of the pods folder

```ruby
  shapepods = "../shapewatchpods/"

  # Pods for ShaweWatch Integration
  pod "React", :path => "#{shapepods}react-native"
  pod "React/Core", :path => "#{shapepods}react-native"
  pod "React/Core", :path => "#{shapepods}react-native"
  pod "React/CxxBridge", :path => "#{shapepods}react-native"
  pod "React/DevSupport", :path => "#{shapepods}react-native"
  pod "React/RCTActionSheet", :path => "#{shapepods}react-native"
  pod "React/RCTAnimation", :path => "#{shapepods}react-native"
  pod "React/RCTGeolocation", :path => "#{shapepods}react-native"
  pod "React/RCTImage", :path => "#{shapepods}react-native"
  pod "React/RCTLinkingIOS", :path => "#{shapepods}react-native"
  pod "React/RCTNetwork", :path => "#{shapepods}react-native"
  pod "React/RCTSettings", :path => "#{shapepods}react-native"
  pod "React/RCTText", :path => "#{shapepods}react-native"
  pod "React/RCTVibration", :path => "#{shapepods}react-native"
  pod "React/RCTWebSocket", :path => "#{shapepods}react-native"

  pod "yoga", :path => "#{shapepods}react-native/ReactCommon/yoga"

  pod "DoubleConversion", :podspec => "#{shapepods}react-native/third-party-podspecs/DoubleConversion.podspec"
  pod "glog", :podspec => "#{shapepods}react-native/third-party-podspecs/glog.podspec"
  pod "Folly", :podspec => "#{shapepods}react-native/third-party-podspecs/Folly.podspec"

  pod 'PocketSVG', '~> 2.0'

  pod "RNGGestures", :path => "#{shapepods}touch-gestures"
  pod "react-native-webview", :path => "#{shapepods}webview"
  pod "RNCAsyncStorage", :path => "#{shapepods}async-storage"
  pod "RNGestureHandler", :path => "#{shapepods}gesture-handler"
  pod "RNDeviceInfo", :path => "#{shapepods}device-info"
  pod "RNKeychain", :path => "#{shapepods}keychain"
  pod "ReactNativePermissions", :path => "#{shapepods}permissions"
  pod "RNFS", :path => "#{shapepods}fs"
  pod "Picker", :path => "#{shapepods}picker"
  pod "react-native-orientation-locker", :path => "#{shapepods}orientation-locker"
  pod "RNSVG", :path => "#{shapepods}svg"
  pod "BVLinearGradient", :path => "#{shapepods}linear-gradient"
  pod "RNLocalize", :path => "#{shapepods}localize"
  pod "RNShare", :path => "#{shapepods}share"
  pod "react-native-sqlite-storage", :path => "#{shapepods}sqlite-storage"
  pod "react-native-netinfo", :path => "#{shapepods}netinfo"
  pod "react-native-geolocation", :path => "#{shapepods}geolocation"
  pod "RCTSystemSetting", :path => "#{shapepods}system"
  pod "QuantaCorp", :path => "#{shapepods}QC"

  pod "ShapeWatch", :path => "#{shapepods}ShapeWatch"

  post_install do |installer|
    installer.pods_project.targets.each do |target|
      target.build_configurations.each do |config|
        config.build_settings['ENABLE_BITCODE'] = 'NO'
      end
    end
  end
```

### Copy files to XCode

Copy all the files included in the CopyToXcode folder to your project.

### How to use?

Set the username and password of the user and clientId, clientSecret for your App

```swift
let initialProperties: NSDictionary = ["username": <username>, "password": <password>, "clientId": <clientId>, "clientSecret": <clientSecret>]
let rootView = SWBridge.sharedInstance.viewForModule(initialProperties: initialProperties as? [String : String])
let vc = UIViewController()
vc.view = rootView
present(vc, animated: true, completion: nil)
```

### Add

There are some usage descriptions that have to be included in the Info.plist file for it to be allowed up on the store

Used by ShapeWatch

- NSCameraUsageDescription: To be able to do a mobile scan

Not used by ShapeWatch but Apple still needs them.

- NSAppleMusicUsageDescription
- NSBluetoothPeripheralUsageDescription
- NSCalendarsUsageDescription
- NSContactsUsageDescription
- NSLocationAlwaysUsageDescription
- NSLocationWhenInUseUsageDescription
- NSMotionUsageDescription
- NSPhotoLibraryUsageDescription
- NSSpeechRecognitionUsageDescription

## KNOWN ISSUES

- Bitcode has to be disabled to archive the app
